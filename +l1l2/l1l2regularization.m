function [out] = l1l2regularization(X, y, mu, tau, beta, kmax, tolerance, return_iterations, adaptive)
% L1L2REGULARIZATION Implementation of the Fast Iterative Shrinkage-Thresholding Algorithm 
% to solve a least squares problem with `l1l2` penalty. 
%
%   out = L1L2REGULARIZATION(X, y, mu, tau)
%   out = L1L2REGULARIZATION(X, y, mu, tau, beta)
%   out = L1L2REGULARIZATION(X, y, mu, tau, beta, kmax)
%   out = L1L2REGULARIZATION(X, y, mu, tau, beta, kmax, tolerance)
%   out = L1L2REGULARIZATION(X, y, mu, tau, beta, kmax, tolerance, return_iterations)
%   out = L1L2REGULARIZATION(X, y, mu, tau, beta, kmax, tolerance, return_iterations, adaptive)
%
%   MATLAB implementation of l1l2py.algoritms.l1l2_regularization.

if ~exist('beta', 'var') || isempty(beta)
    beta = [];
end
if ~exist('kmax', 'var') || isempty(kmax)
    kmax = 1e5;
end
if ~exist('tolerance', 'var') || isempty(tolerance)
    tolerance = 1e-5;
end
if ~exist('return_iterations', 'var') || isempty(return_iterations)
    return_iterations = 0;
end
if ~exist('adaptive', 'var') || isempty(adaptive)
    adaptive = 0;
end

if return_iterations
    out = struct('beta',[],'k',[]);
else
    out = struct('beta',[]);
end

% -- l1l2py.algorithms.l1l2_regularization reimplementation starts here -- %

[n,d] = size(X);

% beta starts from 0 and we assume also that the previous value is 0
if isempty(beta)
    beta = zeros(d,1);
end

if n>d
    XTY = X'*y;
else
    XTY = 0; % not used
end

% First iteration with standard sigma
sigma = l1l2.getSigma(X, mu); % l1l2py.algorithms._sigma()
if sigma < realmin('double') % is zero...
    out.beta = zeros(1,d);
    if return_iterations
        out.k = 0;
    end
end

mu_s = mu /sigma;
tau_s = tau / (2.0 * sigma);
nsigma = n * sigma;

% Starting conditions
aux_beta = beta;
t = 1.0;

for k = 1:kmax
    % Pre-calculated "heavy" computation
    if n>d
        precalc = XTY - X'*(X*aux_beta);
    else
        precalc = X'*(y-X*aux_beta);
    end
    
    % Soft-Thresholding
    value = (precalc / nsigma) + ((1.0 - mu_s) * aux_beta);
    beta_next = sign(value) .* l1l2.clip(abs(value) - tau_s, 0, realmax('double'));
    
    %%%%%%%%%%% Adaptive step size starts here %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if adaptive 
        beta_diff = aux_beta - beta_next;
        
        % Only if there is an increment of the solution
        % we can calculate the adaptive step-size
        if sum(beta_diff)
            tmp = X*beta_diff; % <-- adaptive-step-size drawback
            num = (tmp'*tmp)./n;
            
            sigma = num / (beta_diff'*beta_diff);
            mu_s = mu / sigma;
            tau_s = tau/ (2.0 * sigma);
            nsigma = n * sigma;
            
            % Soft-Thresholding
            value = (precalc / nsigma) + ((1.0 - mu_s) * aux_beta);
            beta_next = sign(value) .* l1l2.clip(abs(value) - tau_s, 0, realmax('double'));
        end
    end
    %%%%%%%%%%% Adaptive step size ends here %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%% FISTA starts here %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    beta_diff = beta_next - beta;
    t_next = 0.5 * (1.0 + sqrt(1.0 + 4.0 *t^2));
    aux_beta = beta_next + ((t - 1.0)/t_next)*beta_diff;
    
    % Convergence values
    max_diff = max(abs(beta_diff));
    max_coef = max(abs(beta_next));
    
    % Values update
    t = t_next;
    beta = beta_next;
    
    % Stopping rule
    if max_coef == 0.0 || (max_diff / max_coef) <= tolerance, break; end
    %%%%%%%%%%% FISTA ends here %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

out.beta = beta;
if return_iterations
    out.k = k;
else
    out.k = 0; % not used
end

% -- l1l2py.algorithms.l1l2_regularization reimplementation starts here -- %

end

