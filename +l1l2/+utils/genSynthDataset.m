function [X,y,beta] = genSynthDataset(method, varargin)
%GENSYNTHDATASET Generate synthetic regression problem.
%
%   This function works as wrapper for all the dataset creation utilities of the current package.
%
% USAGE:
%   [X,y,beta] = genSynthDataset(method, varargin)
%
% INPUT ARGUMENTS:
% method    dataset creation method: 'sparse', 'correlated', 'blockcorrelated', 'zouhastie'.
%           See the correspondent function documentation of this package.
% varargin  input arguments of the selected method
%

switch lower(method)
    case 'sparse'
        [X,y,beta] = l1l2.utils.genSparseDataset(varargin{:});
    case 'correlated'
        [X,y,beta] = l1l2.utils.genCorrelatedDataset(varargin{:});
    case 'blockcorrelated'
        [X,y,beta] = l1l2.utils.genBlockCorrelatedDataset(varargin{:});
    case 'zouhastie'
        [X,y,beta] = l1l2.utils.genZouHastie2005d(varargin{:});
    case 'null'
        [X,y,beta] = l1l2.utils.genNullDataset(varargin{:});
    otherwise
        error('Unknown method %s.',method)
end

%fprintf('%s dataset created\n',method)
