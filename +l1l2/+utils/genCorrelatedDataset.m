function [X,y,beta] = genCorrelatedDataset(n, p, k, amplitude, sigma, rho, seed)
%GENCORRELATEDDATASET Generate a synthetic LINEARLY CORRELATED regression problem.
%
% USAGE:
%   [X,y,beta] = genCorrelatedDataset(n, p, k)
%   [X,y,beta] = genCorrelatedDataset(n, p, k, amplitude)
%   [X,y,beta] = genCorrelatedDataset(n, p, k, amplitude, sigma)
%   [X,y,beta] = genCorrelatedDataset(n, p, k, amplitude, sigma, rho)
%   [X,y,beta] = genCorrelatedDataset(n, p, k, amplitude, sigma, rho, seed)
%
% INPUT ARGUMENTS:
% n           number of samples
% p           total number of features
% k           total number of features with coefficient different from zero
% amplitude   amplitude of the k features
% sigma       noise variance
% rho         correlation level, Sigma(i,j) = rho^abs(i-j)
% seed        random seed
%
if ~exist('seed', 'var') || isempty(seed)
    rng('shuffle');
else
    rng(seed);
end
if ~exist('sigma', 'var') || isempty(sigma)
    sigma = 1;
end
if ~exist('amplitude', 'var') || isempty(amplitude)
    amplitude = 3.5;
end
if ~exist('rho', 'var') || isempty(rho)
    rho = 0.5;
end
% --------------------------------------------------------- %

% Correlation matrix
Sigma = zeros(p,p);
for i = 1:p
    for j = 1:p
        Sigma(i,j) = rho^abs(i-j);
    end
end

% Multi-variate  normal random numbers
X = mvnrnd(zeros(n,p),Sigma)/ sqrt(n);

S0 = randsample(p,k);
beta = zeros(p,1);
beta(S0) = amplitude;
beta = beta .* sign(randn(p,1)); % {+- amplitude}
y = X*beta + sigma .* randn(n,1);
end

