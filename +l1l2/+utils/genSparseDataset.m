function [X,y,beta] = genSparseDataset(n, p, k, amplitude, sigma, seed)
%GENSPARSEDATASET Generate a synthetic sparse LINEAR regression problem.
%
% USAGE:
%   [X,y,beta] = genCorrelatedDataset(n, p, k)
%   [X,y,beta] = genCorrelatedDataset(n, p, k, amplitude)
%   [X,y,beta] = genCorrelatedDataset(n, p, k, amplitude, sigma)
%   [X,y,beta] = genCorrelatedDataset(n, p, k, amplitude, sigma, seed)
%
% INPUT ARGUMENTS:
% n           number of samples
% p           total number of features
% k           total number of features with coefficient different from zero
% amplitude   amplitude of the k features
% sigma       noise variance
% seed        random seed
%
if ~exist('seed', 'var') || isempty(seed)
    rng('shuffle');
else
    rng(seed);
end
if ~exist('amplitude', 'var') || isempty(amplitude)
    amplitude = 3.5;
end
if ~exist('sigma', 'var') || isempty(sigma)
    sigma = 1;
end

% --------------------------------------------------------- %

X = randn(n,p)/ sqrt(n);
S0 = randsample(p,k);
beta = zeros(p,1);
beta(S0) = amplitude;
beta = beta .* sign(randn(p,1)); % {+- amplitude}
y =  X*beta + sigma .* randn(n,1);
end

