function [X,y,beta] = genNullDataset(n, p, varargin)
%GENNULLDATASET Generate a synthetic problem with random labels.
%
% USAGE:
%   [X,y,beta] = genCorrelatedDataset(n, p)
%
% INPUT ARGUMENTS:
% n           number of samples
% p           total number of features
%

if ~exist('seed', 'var') || isempty(seed)
    rng('shuffle');
else
    rng(seed);
end

% --------------------------------------------------------- %

X = randn(n,p)/sqrt(n);
y = -1 + 2.*rand(n,1);
beta = zeros(p,1); % added for consistency only
end

