function [X,y,beta] = genZouHastie2005d(n, p, k, amplitude, sigma, seed)
%GENSYNTHDATASET Generate a synthetic regression problem as in Zou Hastie
%2005 scenario (d).
%
% Generate a linear regression problem generated as follows:
%   
%   y = X * beta + noise
%
% where beta = (a,...,a, 0,...,0):
%               |------| |-------|
%                  k       p - k
%
% X is a n x p matrix like: X = [G1, G2, G3, G4] where Gj are
% groups of features built as follows:
%   - Z1, Z2, Z3 ~ N(0,1)
%   - group 1 (G1): (signal) Z1 + (noise) ~ N(0,sigma) -> n x k/3
%   - group 2 (G2): (signal) Z2 + (noise) ~ N(0,sigma) -> n x k/3
%   - group 3 (G3): (signal) Z3 + (noise) ~ N(0,sigma) -> n x k/3
%   - group 4 (G4): ~ N(0,1) -> n x p-k
%
% USAGE:
%   [X,y,beta] = genZouHastie2005d(n, p, k)
%   [X,y,beta] = genZouHastie2005d(n, p, k, amplitude)
%   [X,y,beta] = genZouHastie2005d(n, p, k, amplitude, sigma)
%   [X,y,beta] = genZouHastie2005d(n, p, k, amplitude, sigma, seed)
%
% INPUT ARGUMENTS:
% n           number of samples
% p           total number of features
% k           total number of features with coefficient different from
%             zero (k must be integer multiple of 3. If it is not, then
%             k is forced to k - mod(k,3) ).
% amplitude   amplitude of the k features
% seed        random seed
%
% REFERENCE:
% Zou and Hastie (2005), Regularization and variable selection via the elastic
% net.
%

if ~exist('amplitude', 'var') || isempty(amplitude)
    amplitude = 3;
end
if ~exist('sigma', 'var') || isempty(sigma)
    sigma = 1;
end
if ~exist('seed', 'var') || isempty(seed)
    rng('shuffle');
else
    rng(seed);
end
% --------------------------------------------------------- %

% Make k integer multiple of 3
r = mod(k,3);
if r ~= 0
    k = k - r;
end

% Generate grouped variables
Z = randn(n,3);
G1 = repmat(Z(:,1),1,k/3) + 0.01 .* randn(n,k/3);
G2 = repmat(Z(:,2),1,k/3) + 0.01 .* randn(n,k/3);
G3 = repmat(Z(:,3),1,k/3) + 0.01 .* randn(n,k/3);
G4 = randn(n,p-k);
X = [G1 G2 G3 G4] / sqrt(n);

% Generate features
beta = zeros(p,1);
beta(1:k) = amplitude;
beta = beta .* sign(randn(p,1)); % {+- amplitude}

% Generate labels
y = X*beta + sigma .* randn(n,1);

end
