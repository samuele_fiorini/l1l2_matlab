function [X,y,beta] = genBlockCorrelatedDataset(n, p, k, amplitude, sigma, rho, seed)
%GENBLOCKCORRELATEDDATASET Generate a synthetic LINEARLY BLOCK CORRELATED regression problem.
%
% USAGE:
%   [X,y,beta] = genBlockCorrelatedDataset(n, p, k)
%   [X,y,beta] = genBlockCorrelatedDataset(n, p, k, amplitude)
%   [X,y,beta] = genBlockCorrelatedDataset(n, p, k, amplitude, sigma)
%   [X,y,beta] = genBlockCorrelatedDataset(n, p, k, amplitude, sigma, rho)
%   [X,y,beta] = genBlockCorrelatedDataset(n, p, k, amplitude, sigma, rho, seed)
%
% INPUT ARGUMENTS:
% n           number of samples
% p           total number of features of which p-k are correlated
% k           total number of (correlated) features with coefficient different from zero
% amplitude   amplitude of the k features
% sigma       noise variance
% rho         correlation level, Sigma(i,j) = rho^abs(i-j)
% seed        random seed
%
if ~exist('seed', 'var') || isempty(seed)
    rng('shuffle');
else
    rng(seed);
end
if ~exist('sigma', 'var') || isempty(sigma)
    sigma = 1;
end
if ~exist('amplitude', 'var') || isempty(amplitude)
    amplitude = 3.5;
end
if ~exist('rho', 'var') || isempty(rho)
    rho = 0.5;
end
% --------------------------------------------------------- %

% Correlation matrix
Sigma = zeros(k,k);
for i = 1:k
    for j = 1:k
        Sigma(i,j) = rho^abs(i-j);
    end
end

% Multi-variate  normal random numbers
X = [mvnrnd(zeros(n,k),Sigma) randn(n,p-k)] / sqrt(n);

beta = zeros(p,1);
beta(1:k) = amplitude;
beta = beta .* sign(randn(p,1)); % {+- amplitude}
y = X*beta + sigma .* randn(n,1);
end

