function b = clip(a, low, high)
%CLIP Clip (limit) the values in an array
%
    b = a;
    b(a<low) = low;
    b(a>high) = high;
end



