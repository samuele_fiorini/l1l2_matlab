function sigma = getSigma(X, mu)
%GETSIGMA 
%   MATLAB implementation of l1l2py.algoritms._sigma.

[n,p] = size(X);

if p > n
    tmp = X*X';
else
    tmp = X'*X;
end

[~,S,~] = svd(tmp);
sigma = S(1,1)/n + mu;

end

