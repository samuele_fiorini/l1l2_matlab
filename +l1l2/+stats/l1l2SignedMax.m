function [W, Z] = l1l2SignedMax(X, X_ko, y, nlambda, mu)
% L1L2SIGNEDMAX  The signed maximum l1l2 statistic W
%   [W, Z] = L1L2SIGNEDMAX(X, X_ko, y)
%   [W, Z] = L1L2SIGNEDMAX(X, X_ko, y, nlambda)
%   [W, Z] = L1L2SIGNEDMAX(X, X_ko, y, nlambda, mu)
%
%   Computes the l1l2 statistic:
%
%     W_j = max(Z_j, \tilde Z_j) * sgn(Z_j - \tilde Z_j).
%
%   Here Z_j and \tilde Z_j are the maximum valued of the regularization
%   parameter lambda at which the jth variable and its knockoff,
%   respectively, enter the l1l2 model.
%
%   An handler of this function can be fed to knockoff.filter(...,'Statistic')

if ~exist('nlambda', 'var') || isempty(nlambda), nlambda = []; end
if ~exist('mu', 'var') || isempty(mu), mu = []; end

Z = l1l2.stats.l1l2MaxLambda([X X_ko], y, nlambda, mu);

p = size(X,2);
orig = 1:p; ko = (p+1):(2*p);
W = max(Z(orig), Z(ko)) .* sign(Z(orig) - Z(ko));

end

