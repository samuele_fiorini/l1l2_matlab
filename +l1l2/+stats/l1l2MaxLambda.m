function first_lambda = l1l2MaxLambda(X, y, nlambda, mu)
% L1L2MAXLAMBDA  Maximum lambda's for which variables in l1l2 model
%   maxLambda = L1L2MAXLAMBDA(X, y)
%   maxLambda = L1L2MAXLAMBDA(X, y, nlambda)
%   maxLambda = L1L2MAXLAMBDA(X, y, nlambda, mu)
%
%   For each variable (column in X), computes the maximum value of lambda 
%   at which the variable enters in the l1l2 model:
%   
%       1/n |y - X beta|^2 + mu |beta|_2 + lambda |beta|_1
%
%   the functional above is minimized using FISTA. If mu is zero it boils
%   down to a simple lasso. If mu is not specified, then it will be estimated
%   using an heuristic on the eigenvalues span of the estimated covariance matrix.

[n,p] = size(X);
if ~exist('nlambda', 'var') || isempty(nlambda)
    nlambda = 5*p;
end
if ~exist('mu', 'var') || isempty(mu)
    % Define mu
    % [~,D] = eig(X*X'); % this would be the case if n < p (not for Knockoffs)
    [~,D] = eig(X'*X);
    mu = (max(diag(D)) + min(diag(D)))/(2*n); % l1l2signature-like
    fprintf('Using mu = %2.5f\n', mu)
end

% Define lambda range
lambda_max = 2*max(abs(X'*y))/n; % l1l2py-like
% lambda_max = max(abs(X'*y))/n; % glmnet-like !!
lambda_min = lambda_max/(2*1e3);
k = (0:(nlambda-1))/nlambda;
lambda_range = lambda_max .* (lambda_min/lambda_max).^k;

% Evaluate l1l2regularization
beta = zeros(p,nlambda);
parfor i = 1:nlambda
    lambda = lambda_range(i);
    outl1 = l1l2.l1l2regularization(X, y, mu, lambda);     % l1 step    
    beta(:,i) = outl1.beta;
end

first_lambda = zeros(1,p);
for j = 1:p,
    first_time = find(abs(beta(j,:)) > 0, 1, 'first');
    if isempty(first_time),
        first_lambda(j) = 0;
    else
        first_lambda(j) = lambda_range(first_time);
    end
end

end
