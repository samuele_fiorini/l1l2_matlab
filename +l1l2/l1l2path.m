function out = l1l2path(X, y, mu, tau_range, beta, kmax, tolerance, adaptive)
% L1L2PATH  Efficient solution of different `l1l2` regularization problems on increasing 
% values of the `l1-norm` parameter. 
%   
%   out = L1L2PATH(X, y, mu, tau_range)
%   out = L1L2PATH(X, y, mu, tau_range, beta)
%   out = L1L2PATH(X, y, mu, tau_range, beta, kmax)
%   out = L1L2PATH(X, y, mu, tau_range, beta, kmax, tolerance)
%   out = L1L2PATH(X, y, mu, tau_range, beta, kmax, tolerance, adaptive)
%   
%   MATLAB implementation of l1l2py.algoritms.l1l2_path.

if ~exist('beta', 'var') || isempty(beta)
    beta = [];
end
if ~exist('kmax', 'var') || isempty(kmax)
    kmax = 1e5;
end
if ~exist('tolerance', 'var') || isempty(tolerance)
    tolerance = 1e-5;
end
if ~exist('adaptive', 'var') || isempty(adaptive)
    adaptive = 0;
end

return_iterations = 0; % As in l1l2py
% -- l1l2py.algorithms.l1l2_path reimplementation starts here -- %

[n,p] = size(X);

if mu == 0.0
    beta_ls = ridge(X,y,0);
end
if isempty(beta)
    beta = zeros(p,1);
end

out = [];
nonzero = 0;
for tau = sort(tau_range, 'descend')
    if mu == 0.0 && nonzero >= n % lasso solution
        beta_next = beta_ls;
    else
        l1l2_out = l1l2.l1l2regularization(X, y, mu, tau, beta, kmax, tolerance, return_iterations ,adaptive);
        beta_next = l1l2_out.beta; 
    end
    
    nonzero = sum(beta_next ~= 0);
    if nonzero > 0
        out = [out beta_next];
    end
    
    beta = beta_next;
    
end

% -- l1l2py.algorithms.l1l2_path reimplementation ends here -- %

end

