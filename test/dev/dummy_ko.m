qwe
clc
% This is the demo used for l1l2 statistics function development

%% Synthetic problem parameters

n = 4;          % Number of data points
p = 2;          % Number of variables
k = 1;           % Number of variables with nonzero coefficients
amplitude = 3.5;  % Magnitude of nonzero coefficients
q = 0.20;         % Target false discovery rate (FDR)

% [X, y, beta] = l1l2.utils.genSynthDataset3(n, p, k, amplitude, 1, 0.5, []);
[X, y, beta] = l1l2.utils.genSynthDataset(n, p, k, amplitude);
% [X, y, beta] = l1l2.utils.genZouHastie2005d(n, p, k, amplitude);
X_ko1 = knockoff.create(X, 'equi');
X_ko2 = knockoff.create(X, 'SDP');

%%
% figure
% imagesc(X'*X,[0,1])
% title('X^TX')

% %%
% figure
% S_diags1 = X'*X_ko1;
% diags1 = -(S_diags1 - X'*X);
% imagesc(diags1); colorbar()
% s1 = diag(diags1);
% title('Equi diag(s)')
% 
% figure
% S_diags2 = X'*X_ko2;
% diags2 = -(S_diags2 - X'*X);
% imagesc(diags2); colorbar()
% s2 = diag(diags2);
% title('SDP diag(s)')

%% Pearson corr
figure
rXX = corr(X);
imagesc(rXX,[0,1]); colorbar()
title('corr(X,X)')
 
figure
rX_ko1X_ko1 = corr(X_ko1);
imagesc(rX_ko1X_ko1,[0,1]); colorbar()
title('Corr Equi X_ko')

figure
rX_ko2X_ko2 = corr(X_ko2);
imagesc(rX_ko2X_ko2,[0,1]); colorbar()
title('Corr SDP X_{ko}')

figure
rXX_ko1 = corr(X,X_ko1);
imagesc(rXX_ko1,[0,1]); colorbar()
title('Equi corr(X,X_{ko})')

figure
rXX_ko2 = corr(X,X_ko2);
imagesc(rXX_ko2,[0,1]); colorbar()
title('SDP corr(X,X_{ko})')