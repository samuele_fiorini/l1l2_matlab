tau_range = logspace(-5,1,1000);
mu_range = logspace(-5,1,1000);

tau_range = (tau_range-min(tau_range))./(max(tau_range)-min(tau_range));
mu_range = (mu_range-min(mu_range))./(max(mu_range)-min(mu_range));

[T,M] = meshgrid(tau_range,mu_range);
K = 10;
Z = 1./(1 + exp(K*(-T+M)))^2;
Z = (Z-min(Z(:)))./(max(Z(:))-min(Z(:)));

% C = del2(Z);

figure
mesh(T,M,Z)
xlabel('tau')
ylabel('mu')
zlabel('lklh')
title('Likelihood to be good')

figure
imagesc(tau_range,mu_range,Z)
xlabel('tau')
ylabel('mu')
title('Likelihood to be good')

