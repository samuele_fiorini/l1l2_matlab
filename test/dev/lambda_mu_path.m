qwe
clc
% This is the demo used for l1l2 statistics function development

%% Synthetic problem parameters

n = 600;          % Number of data points
p = 150;          % Number of variables
k = 30;           % Number of variables with nonzero coefficients
amplitude = 3.5;  % Magnitude of nonzero coefficients
sigma = 1;        % Noise level
q = 0.20;         % Target false discovery rate (FDR)

seed = [];

trueDiscoveries = @(S,beta) sum(beta(S) > 0);
FDP = @(S,beta) sum(beta(S) == 0) / max(1, length(S));
printSummary = @(S,beta) fprintf(...
    ['%d discoveries (%d True, %d False)\n' ...
     'FDP = %2.2f%% (target FDR = %2.f%%)\n\n'], ...
    size(S,2), trueDiscoveries(S,beta), size(S,2)-trueDiscoveries(S,beta),100*FDP(S,beta), 100*q);

[X, y, beta] = genZouHastie2005d(n, p, k, amplitude, [], seed);

%% Evaluate and visualize lambda vs mu path

% Normalize dataset
% X = knockoff.private.normc(X); % BE CAREFUL, FIX THIS!!

% Split dataset
test_frac = 0.33;
n_test = round(n*test_frac);
n_train = n - n_test;
[Xtr, ytr, Xts, yts] = randomSplitDataset(X, y, n_train, n_test);

% Grid dimension
nlambda = 200;
nmu = 100;

% Define lambda range
lambda_max = 2*max(abs(Xtr'*ytr))/n; % l1l2py-like
lambda_min = lambda_max/(2*1e3);
k = (0:(nlambda-1))/nlambda;
lambda_range = sort(lambda_max .* (lambda_min/lambda_max).^k);

% Define mu range
[~,D] = eig(Xtr'*Xtr);
mu_max = (max(diag(D)) + min(diag(D)))/(2*n); % l1l2signature-like
mu_min = mu_max/(2*1e3);
k = (0:(nmu-1))/nmu;
mu_range = sort(mu_max .* (mu_min/mu_max).^k);

% Nested path loop
Betas   = zeros(nmu,nlambda,p);
ts_err = zeros(nmu,nlambda);
tr_err = zeros(nmu,nlambda);
lm_selected = zeros(nmu,nlambda);
for i = 1:nmu
    mu = mu_range(i);
    parfor j = 1:nlambda
        lambda = lambda_range(j);
        l1 = l1l2.l1l2regularization(Xtr, ytr, mu, lambda); % l1 step
        selected = find(abs(l1.beta)>0);                    % feature selection
        beta = ridge(ytr,Xtr(:,selected),mu);               % l2 step
        yhat_ts = Xts(:,selected)*beta;
        yhat_tr = Xtr(:,selected)*beta;
        ts_err(i,j) = sum((yts - yhat_ts).^2)/n;              % AVGRSS(mu,lambda)
        tr_err(i,j) = sum((ytr - yhat_tr).^2)/n;                
        Betas(i,j,:) = l1.beta;
        lm_selected(i,j) = sum(abs(l1.beta) > 0);
    end
    fprintf('%d)\tmu %2.3f done\n',i,mu)
end

%% 3D err plot
figure
% [xx,yy] = meshgrid(mu_range,lambda_range);
% surf(xx,yy,ts_err); hold on

[xx,yy] = meshgrid(lambda_range,mu_range);
surf(xx,yy,ts_err)
surf(xx,yy,tr_err)

legend('ts','tr')
% xlabel('lambda')
% ylabel('mu')
zlabel('RSS')
title('test error')
% 
% % find the min
% ind = find(ts_err == min(ts_err(:)));
% [r,c] = ind2sub(size(ts_err),ind);
% 
% for ii=1:length(ind)
%     scatter3(mu_range(r(ii)),lambda_range(c(ii)),min(ts_err(:)),100,'filled')
% end

hold off

%%
figure
% [xx,yy] = meshgrid(mu_range,lambda_range);
zz = abs(Betas(:,:,31));

% zz = (zz - min(zz(:)))./(max(zz(:)) - min(zz(:)));

imagesc(lambda_range, mu_range, zz)
set(gca, 'Ydir', 'Normal')
% ax = gca;
% ax.XTick = lambda_range;
% ax.YTick = mu_range;
% surf(xx,yy,zz)

ylabel('mu')
xlabel('lambda')
colorbar()

figure
plot(lambda_range,squeeze(abs(Betas(:,:,31)))); xlabel('l');
%%
figure

imagesc(lambda_range, mu_range, lm_selected)
set(gca, 'Ydir', 'Normal')
ylabel('mu')
xlabel('lambda')
colorbar()

%%
[xx,yy] = meshgrid(lambda_range,mu_range);
surf(xx,yy,lm_selected)
ylabel('mu')
xlabel('lambda')
