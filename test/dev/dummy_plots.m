% %% 3D beta plot
% [xx,yy] = meshgrid(lambda_range,mu_range);
% figure
% 
% for idx = 1:p
%     B = squeeze(Betas(:,:,idx));
%     surf(xx,yy,B); hold on
% end
% xlabel('lambda')
% ylabel('mu')
% % title(['Beta [', num2str(idx), ']'])
% hold off

% %% 2D beta plot
% figure
% for i = 1:nmu
%     mu = mu_range(i);
%     for s = 1:p % each feature
%         plot(lambda_range, squeeze(Betas(i,:,s))); hold on
%     end
% end
% hold off
% xlabel('lambda')
% ylabel('Beta')
