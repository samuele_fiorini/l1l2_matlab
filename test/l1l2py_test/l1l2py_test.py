#!/usr/bin/python

import numpy as np
import pandas as pd
from scipy.io import loadmat
import l1l2py

def make_csv(X,y):
    sample_names = ['id_'+str(i) for i in np.arange(X.shape[0])]
    feat_names = ['feat_'+str(i) for i in np.arange(X.shape[1])]

    dfX = pd.DataFrame(data = X, columns = feat_names, index = sample_names)
    dfy = pd.DataFrame(data = y, columns = ['class'], index = sample_names)

    dfX.to_csv('X.csv')
    dfy.to_csv('y.csv')


def main():
    # Load data
    X = loadmat('X.mat')['X']
    y = loadmat('y.mat')['y']

    # Make csv
    # make_csv(X,y)

    # # Test sigma --> OK
    # sigma = l1l2py.algorithms._sigma(X,1)
    # print("l1l2py sigma = {}".format(sigma))

    # Test l1l2_regularization
    mu = 0.00
    tau = 5.795817936894919e-05
    beta, k = l1l2py.algorithms.l1l2_regularization(X, y, mu, tau, return_iterations=True)
    print("After {} iteration\nbeta =\n {}\nfeature selected = {}\nsum |beta| = {}".format(k,beta[:10],len(np.where(beta!=0)[0]),np.sum(np.abs(beta))))

    # Test l1l2_path
    # beta_path = tau_range = np.logspace(-3,0,10);
    # return beta





if __name__ == '__main__':
    main()
