qwe
clc
% This is the demo used for l1l2 statistics function development

%% Synthetic problem parameters

n = 600;          % Number of data points
p = 200;          % Number of variables
k = 30;           % Number of variables with nonzero coefficients
amplitude = 3.5;  % Magnitude of nonzero coefficients
q = 0.20;         % Target false discovery rate (FDR)

% seed = 45678;       % Random seed
seed = [];

trueDiscoveries = @(S,Rbeta) sum(Rbeta(S) > 0);
FDP = @(S,Rbeta) sum(Rbeta(S) == 0) / max(1, length(S));
printSummary = @(S,Rbeta) fprintf(...
    ['%d discoveries (%d True, %d False)\n' ...
     'FDP = %2.2f%% (target FDR = %2.f%%)\n'], ...
    size(S,2), trueDiscoveries(S,Rbeta), size(S,2)-trueDiscoveries(S,Rbeta),100*FDP(S,Rbeta), 100*q);

% [X, y, beta] = l1l2.utils.genZouHastie2005d(n, p, k, amplitude, 1, seed);
% [X, y, beta] = l1l2.utils.genSynthDataset(n, p, k, amplitude, 1, seed);
[X, y, beta] = l1l2.utils.genSynthDataset2(n, p, k, amplitude, 1, 0.5, seed);
% [X, y, beta] = l1l2.utils.genSynthDataset3(n, p, k, amplitude, 1, 0.5, seed);

%% l1l2 stats
FISTAl1l2SignedMax = @(X, X_ko, y) ...
    l1l2.stats.l1l2SignedMax(X, X_ko, y, [], []);

FISTAl1SignedMax = @(X, X_ko, y) ...
    l1l2.stats.l1l2SignedMax(X, X_ko, y, [], 0);

%% Breakdown

% X_ko = knockoff.create(X);
% [X_ko, ~, ~] = l1l2.utils.genZouHastie2005d(n, p, k, amplitude, 1, seed);
[X_ko, ~, ~] = l1l2.utils.genSynthDataset2(n, p, k, amplitude, 1, 0.5, seed);
[W, Z] = FISTAl1l2SignedMax(X, X_ko, y);
% [W, Z] = knockoff.stats.lassoSignedMax(X, X_ko, y);
t = knockoff.threshold(W, q);

%% Plot
S0 = find(abs(beta)>0);

fig = figure();
hold on
set(fig, 'DefaultTextInterpreter', 'latex');
gscatter(Z(1:p), Z(p+1:2*p), ismember(1:p, S0), 'kr');
plot([t t 0], [0 t t], 'k');
hold off

xlabel('Value of $\lambda$ when $X_j$ enters model');
ylabel('Value of $\lambda$ when $\tilde X_j$ enters model');
limits = [0 max(Z)];
xlim(limits); ylim(limits);
title('Knockoff Filter with l1l2 Statistic');
legend('Null feature', 'Non-null feature');
line = refline(1,0);
set(line, 'LineStyle', ':', 'Color', 'black');
