function [c] = concordance(S1,S2)
%CONCORDANCE Return the concordance between two feature selections in terms
%of Jaccard distance.

intr = intersect(S1,S2);
uni  = union(S1,S2);

if ~ isempty(intr)
    c = size(intr,2) / size(uni,2);
else
    c = 0;
end

fprintf('Concordance level: %2.3f %%',c*100)

end

