% Define mu
% [~,D] = eig(X*X'); % that's the case when n < p (not for Knockoffs)
% [~,D] = eig(X'*X);
% mu = (max(diag(D)) - min(diag(D)))/(2*n); % l1l2py style
%log_mu_max = 2*log(max(diag(D))/n);
%log_mu_max = log(max(diag(D))/n);
% log_mu_max = log((max(diag(D)) - min(diag(D)))/(2*n)); % l1l2py style
% log_mu_min = log_mu_max/log(2*1e3);
% mu_range = logspace(log_mu_min, log_mu_max, nmu);
% if one_mu_flag,
% %     mu = mu_range(round(size(mu_range,2)/2)); 
%     mu = max(mu_range);
%     fprintf('\nUsing mu = %2.5f\n', mu)
% end

% Evaluate l1l2regularization
beta = zeros(p,nlambda);
parfor i = 1:nlambda
% for i = 1:nlambda
    lambda = lambda_range(i);
    outl1 = l1l2.l1l2regularization(X, y, mu, lambda);     % l1 step
%     selected = abs(betal1.beta) > 0;                     % feature selection
%     betal1l2 = ridge(y, X(:,selected), mu);              % l2 step
%     beta = [beta outl1.beta];
    beta(:,i) = outl1.beta;
end

