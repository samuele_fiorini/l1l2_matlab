% This demo plots the target FDR vs the average FDP when the knockoff
% filter is applied to a synthetic data set.

%% Synthetic problem parameters

n = 600;          % Number of data points
p = 200;          % Number of variables
k = 30;           % Number of variables with nonzero coefficients

% n = 300;          % Number of data points
% p = 100;          % Number of variables
% k = 15;           % Number of variables with nonzero coefficients

amplitude = 3.5;  % Magnitude of nonzero coefficients
sigma = 1;        % Noise level
q = 0.20;         % Target false discovery rate (FDR)

% seed = 45678;       % Random seed
seed = [];

trueDiscoveries = @(S,beta) sum(beta(S) > 0);
FDP = @(S,beta) sum(beta(S) == 0) / max(1, length(S));
printSummary = @(S,beta) fprintf(...
    ['%d discoveries (%d True, %d False)\n' ...
     'FDP = %2.2f%% (target FDR = %2.f%%)\n\n'], ...
    size(S,2), trueDiscoveries(S,beta), size(S,2)-trueDiscoveries(S,beta),100*FDP(S,beta), 100*q);

% [X, y, beta] = genZouHastie2005d(n, p, k, amplitude, [], seed);
[X, y, beta] = genSynthDataset(n, p, k, amplitude, sigma, seed);
%% Plot target FDR vs achieved FDR

FDP = @(S) sum(beta(S) == 0) / max(1, length(S));

ntrials = 20;
q = 0.05:0.05:0.5;
fdp = zeros(length(q), ntrials);

FISTAl1l2SignedMax = @(X, X_ko, y) ...
    l1l2.stats.l1l2SignedMax(X, X_ko, y, [], []);


for i = 1:length(q)
    parfor j = 1:ntrials
%         S = knockoff.filter(X, y, q(i), 'Knockoffs', 'SDP','Threshold', 'knockoff+');
        S = knockoff.filter(X, y, q(i), 'Statistic', FISTAl1l2SignedMax, ...
                                        'Knockoffs', 'SDP', 'Threshold', 'knockoff+');
        fdp(i,j) = FDP(S);
    end
end

plot(q, mean(fdp,2));
xlabel('Target FDR'), ylabel('Average FDP'), title('False Discovery Rate');
xlim([0 max(q)]), ylim([0 inf]);
line = refline(1,0);
set(line, 'LineStyle', ':', 'Color', 'black');