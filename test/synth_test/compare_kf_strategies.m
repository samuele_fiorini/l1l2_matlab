qwe
%% Synthetic problem parameters

n = 600;          % Number of data points
p = 200;          % Number of variables
k = 30;           % Number of variables with nonzero coefficients
amplitude = 3.5;  % Magnitude of nonzero coefficients
q = 0.20;         % Target false discovery rate (FDR)

%% Generate dataset

[X1, y1, beta1] = l1l2.utils.genZouHastie2005d(n, p, k, amplitude, 1, []);
[X2, y2, beta2] = l1l2.utils.genSynthDataset(n, p, k, amplitude, 1, []);
[X3, y3, beta3] = l1l2.utils.genSynthDataset2(n, p, k, amplitude, 1, 0.5, []);

%% Generate Knockoffs

X_ko1 = knockoff.create(X1);
X_ko2 = knockoff.create(X2);
X_ko3 = knockoff.create(X3);

%% Eval XTY

XTy1 = X1'*y1;
XTy2 = X2'*y2;
XTy3 = X3'*y3;

X_koTy1 = X_ko1'*y1;
X_koTy2 = X_ko2'*y2;
X_koTy3 = X_ko3'*y3;


%% mean RSS

RSS1 = mean((XTy1 - X_koTy1).^2)
RSS2 = mean((XTy2 - X_koTy2).^2)
RSS3 = mean((XTy3 - X_koTy3).^2)

