% qwe
clc
% This is the demo used for l1l2 statistics function development

%% Synthetic problem parameters

n = 600;          % Number of data points
p = 200;          % Number of variables
k = 30;           % Number of variables with nonzero coefficients
amplitude = 3.5;  % Magnitude of nonzero coefficients
q = 0.20;         % Target false discovery rate (FDR)

% seed = 45678;       % Random seed
seed = [];

trueDiscoveries = @(S,Rbeta) sum(Rbeta(S) > 0);
FDP = @(S,Rbeta) sum(Rbeta(S) == 0) / max(1, length(S));
printSummary = @(S,Rbeta) fprintf(...
    ['%d discoveries (%d True, %d False)\n' ...
     'FDP = %2.2f%% (target FDR = %2.f%%)\n'], ...
    size(S,2), trueDiscoveries(S,Rbeta), size(S,2)-trueDiscoveries(S,Rbeta),100*FDP(S,Rbeta), 100*q);

%% repeat experiments
FISTAl1l2SignedMax = @(X, X_ko, y) ...
    l1l2.stats.l1l2SignedMax(X, X_ko, y, [], []);

FISTAl1SignedMax = @(X, X_ko, y) ...
    l1l2.stats.l1l2SignedMax(X, X_ko, y, [], 0);

K = 50;
l1l2_FPD_array = zeros(1,K);
l1l2_D_array = zeros(1,K);
l1l2_TD_array = zeros(1,K);
l1l2_FD_array = zeros(1,K);
l1_FPD_array = zeros(1,K);
l1_D_array = zeros(1,K);
l1_TD_array = zeros(1,K);
l1_FD_array = zeros(1,K);
lasso_FDP_array = zeros(1,K);
lasso_D_array = zeros(1,K);
lasso_TD_array = zeros(1,K);
lasso_FD_array = zeros(1,K);

% Experiment tag:
% path = '../../figures/Correlated/SDP_KF/';
% path = '../../figures/ZouHastie2009/SDP_KF/';
path = './';

for i=1:K
    fprintf('\n--------------------------------\n')
    [X, y, beta] = l1l2.utils.genSynthDataset('sparse', n, p, k, amplitude, sigma, seed);
    
    % --------------------------- l1 --------------------------------- %
    
    S_l1 = knockoff.filter(X, y, q, 'Statistic', FISTAl1SignedMax, ...
                           'Knockoffs', 'SDP', 'Threshold', 'knockoff');

    fprintf('- l1 statistic\n')
    printSummary(S_l1,beta);
    l1_FPD_array(i) = 100 * FDP(S_l1, beta);
    l1_D_array(i) = size(S_l1,2);
    l1_TD_array(i) = trueDiscoveries(S_l1,beta);
    l1_FD_array(i) = l1_D_array(i) - l1_TD_array(i);
    
    % -------------------------- l1l2 --------------------------------- %

    S_l1l2 = knockoff.filter(X, y, q, 'Statistic', FISTAl1l2SignedMax, ...
                             'Knockoffs', 'SDP', 'Threshold', 'knockoff');

    fprintf('- l1l2 statistic\n')
    printSummary(S_l1l2,beta);
    l1l2_FPD_array(i) = 100 * FDP(S_l1l2, beta);
    l1l2_D_array(i) = size(S_l1l2,2);
    l1l2_TD_array(i) = trueDiscoveries(S_l1l2,beta);
    l1l2_FD_array(i) = l1l2_D_array(i) - l1l2_TD_array(i);
    
    % --------------------------- lasso -------------------------------- %
    
    S_lasso = knockoff.filter(X, y, q, 'Statistic', @knockoff.stats.lassoSignedMax, ...
                              'Knockoffs', 'SDP','Threshold', 'knockoff');

    fprintf('- lasso\n')
    printSummary(S_lasso,beta);
    lasso_FDP_array(i) = 100 * FDP(S_lasso,beta);
    lasso_D_array(i) = size(S_lasso,2);
    lasso_TD_array(i) = trueDiscoveries(S_lasso,beta);
    lasso_FD_array(i) = lasso_D_array(i) - lasso_TD_array(i);

    fprintf('l1 vs lasso:\n')
    c = concordance(S_l1, S_lasso);
end
%% 

h1 = figure(1);
plot(1:K,l1_FPD_array,'ob'); hold on
plot(1:K,l1l2_FPD_array,'+r')
plot(1:K,lasso_FDP_array,'*g')
p1 = plot(1:K,mean(l1_FPD_array)*ones(K,1),'-b');
p2 = plot(1:K,mean(l1l2_FPD_array)*ones(K,1),'--r');
p3 = plot(1:K,mean(lasso_FDP_array)*ones(K,1),'-.g');
legend([p1,p2,p3],'l1','l1l2','lasso')
ylim([0 60])
title(sprintf('FDP over %d experiments',K)); hold off
saveas(h1,strcat(path,'scatterplot.png'))
%%

h2 = figure(2);
boxplot([l1_FPD_array; l1l2_FPD_array; lasso_FDP_array]','labels',{'l1','l1l2','lasso'})
ylim([0 60])
title(sprintf('FDP over %d experiments',K))
saveas(h2,strcat(path,'boxplot.png'))
%%
h3 = figure('Color','w');
lasso = [mean(lasso_D_array), mean(lasso_TD_array), mean(lasso_FD_array)];
l1l2 = [mean(l1l2_D_array), mean(l1l2_TD_array), mean(l1l2_FD_array)];
l1 = [mean(l1_D_array), mean(l1_TD_array), mean(l1_FD_array)];
bar([l1; l1l2; lasso],1); hold on
hline = refline([0 30]);
hline.Color = 'r';
hline.LineStyle = '--';
ylim([0,45])
set(gca,'XTickLabel',{'l1','l1l2','lasso'})
legend('Discoveries','True D','False D')
title(sprintf('Average discoveries over %d experiments',K))
saveas(h3,strcat(path,'avgDscvr.png'))
%%
h4 = figure(4);
power = @(GT,TD) mean(TD)/30;
data = [mean(l1_FPD_array)/100, mean(lasso_FDP_array)/100, mean(l1l2_FPD_array)/100;
        power(k,l1_TD_array), power(k,lasso_TD_array), power(k,l1l2_TD_array)];
gs = gscatter(data(1,:),data(2,:),[1,2,3],[],'opv');
set(gs, 'MarkerSize', 12)
set(gs(1), 'MarkerFaceColor', 'r')
set(gs(2), 'MarkerFaceColor', 'c')
set(gs(3), 'MarkerFaceColor', 'g')

xlabel('FDP')
ylabel('Power (TD/GT)')
legend('l1','lasso','l1l2')
axis([0,1,0,1])
saveas(h4,strcat(path,'power.png'))

