function [S, W] = filter_edit(X, y, q, k, amplitude, rho, varargin)
% FILTER  Run the knockoff filter on a data set.
%   [S, W] = FILTER(X, y, q, ...)
%
%   Structured knockoffs, proof of concept.

parser = inputParser;
parser.CaseSensitive = false;
if (~verLessThan('matlab', '8.2')) % R2013b or later
    parser.PartialMatching = false;
end

istable_safe = @(x) ~verLessThan('matlab', '8.2') && istable(x);
parser.addRequired('X', @(x) isnumeric(x) || istable_safe(x));
parser.addRequired('y', @isnumeric);
parser.addRequired('q', @(x) isnumeric(x) && isscalar(x));

parser.addOptional('Statistic', ...
    @knockoff.stats.lassoSignedMax, ...
    @(x) isa(x, 'function_handle'));
parser.addOptional('Knockoffs', []);
parser.addOptional('Threshold', []);
parser.addOptional('Normalize', true, @islogical);
parser.addOptional('Randomize', false, @islogical);
parser.parse(X, y, q, varargin{:});

% Extract variable names.
if (istable_safe(X))
    Xnames = X.Properties.VariableNames;
    X = table2array(X);
else
    Xnames = {};
end

% Verify dimensions.
[n,p] = size(X);
if (n <= p)
    error('knockoff:DimensionError', 'Data matrix must have n > p')
elseif (n < 2*p)
    warning('knockoff:DimensionWarning', ...
        'Data matrix has p < n < 2*p. Augmenting the model with extra rows.');
    [U,~,~] = svd(X);
    U_2 = U(:,(p+1):n);
    sigma = sqrt(mean((U_2'*y).^2)); % = sqrt(RSS/(n-p))
    if (parser.Results.Randomize)
        y_extra = randn(2*p-n,1)*sigma;
    else
        seed = rng(0);
        y_extra = randn(2*p-n,1)*sigma;
        rng(seed);
    end
    y = [y;y_extra];
    X = [X;zeros(2*p-n,p)];
end

% Normalize X, if necessary.
if (parser.Results.Normalize)
    X = knockoff.private.normc(X);
end

% Run the knockoff filter.

%%
% X_ko = knockoff.create(X, parser.Results.Knockoffs, parser.Results.Randomize);
[n,p] = size(X);
% [X_ko, ~, ~] = l1l2.utils.genSynthDataset3(n, p, k, amplitude, [], rho, []);
[X_ko, ~, ~] = l1l2.utils.genZouHastie2005d(n, p, k, amplitude, [], []);
%%
W = parser.Results.Statistic(X, X_ko, y);
S = knockoff.selectVars(W, q, parser.Results.Threshold);

if (~isempty(Xnames))
    S = Xnames(S);
end

end